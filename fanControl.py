import RPi.GPIO as GPIO
from time import sleep
from gpiozero import CPUTemperature
import math
from datetime import datetime

BASE_PIN = 5
TRESHOLD_ON_TEMP = 55
TRESHOLD_OFF_TEMP = 40

fanState = False

#init
print("Init fan control")
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(BASE_PIN, GPIO.OUT, initial=GPIO.LOW)
cpu = CPUTemperature()

def setFan(state):
	global fanState
	GPIO.output(BASE_PIN, GPIO.HIGH if state == True else GPIO.LOW)
	fanState = state

def temp():
	return cpu.temperature

while True:
	t = temp()
	dateTimeObj = datetime.now()
	if t > TRESHOLD_ON_TEMP and fanState == False:
		setFan(True)
		print(str(dateTimeObj) + " [State] " + str(math.floor(t * 10) / 10) + "'C, fan on")
	elif t < TRESHOLD_OFF_TEMP and fanState == True:
		setFan(False)
		print(str(dateTimeObj) + " [State] " + str(math.floor(t * 10) / 10) + "'C, fan off")
	sleep(5)
